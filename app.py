from flask import Flask,request,jsonify,render_template
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import random
import json  
import time
import datetime
app = Flask(__name__)
client = mqtt.Client()

@app.route('/')
def index():
	return render_template("home.html")

if __name__ == '__main__':
	app.run(host='0.0.0.0',debug=False,threaded=True)
	